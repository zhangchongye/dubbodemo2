package com.zcy.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: zcy
 * @Date: 2020/07/31/9:47
 * @Description:
 */
public class ProviderMain {
    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context=
                new ClassPathXmlApplicationContext(new String[] {"applicationProvider.xml"});
        context.start();
        System.out.println("任意键退出");
        System.in.read();

    }
}
