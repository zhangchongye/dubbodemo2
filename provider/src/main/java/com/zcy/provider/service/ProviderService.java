package com.zcy.provider.service;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: zcy
 * @Date: 2020/07/31/9:47
 * @Description:
 */
public interface ProviderService {
    public String sayHello(String str);
}
