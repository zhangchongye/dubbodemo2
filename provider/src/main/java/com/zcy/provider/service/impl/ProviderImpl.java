package com.zcy.provider.service.impl;

import com.zcy.provider.service.ProviderService;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: zcy
 * @Date: 2020/07/31/9:48
 * @Description:
 */
public class ProviderImpl implements ProviderService {

    public String sayHello(String str){
        String returnStr="hello world,str = " + str;
        System.out.println("服务端说："+returnStr);
        return  returnStr;
    }

}
