package com.zcy.consumer;

import com.zcy.provider.service.ProviderService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: zcy
 * @Date: 2020/07/31/10:14
 * @Description:
 */
public class ConsumerMain {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext(
                new String[] {"applicationConsumer.xml"});
        context.start();
        ProviderService providerService=(ProviderService) context.getBean("providerService");
        System.out.println(providerService.sayHello("1212"));

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
